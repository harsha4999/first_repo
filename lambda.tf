resource "aws_iam_role_policy" "lambda_policy" {
  name   = "lambda_policy"
  role   = aws_iam_role.lambda_role.id
  policy = file("IAM/lambda_policy.json")
}


# variable "myregion" {}

# variable "accountId" {}

resource "aws_iam_role" "lambda_role" {
  name               = "lambda_role"
  assume_role_policy = file("IAM/lambda_assume_role_policy.json")
}

data "archive_file" "lambda_zip1" {
  type        = "zip"
  output_path = "output/hello.zip" 
  source_dir = "${path.cwd}/local/Lambda/hello"
}
data "archive_file" "lambda_zip2" {
  type        = "zip"
  output_path = "output/test.zip" 
  source_dir = "${path.cwd}/local/Lambda/test"
}

data "archive_file" "lambda_layer_zip" {
  type        = "zip"
  output_path = "output/lambda_layer_payload.zip" 
  source_dir = "${path.cwd}/local/nodejs"
}

resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = "output/lambda_layer_payload.zip"
  layer_name = "lambda_layer_name"

  compatible_runtimes = ["nodejs12.x"]
}
resource "aws_lambda_function" "test_lambda" {
  
  function_name = "hello"
  filename      = "output/hello.zip" 
  role          = aws_iam_role.lambda_role.arn
  handler       = "hello.handler"
  runtime       = "nodejs12.x"  
  layers = [aws_lambda_layer_version.lambda_layer.arn]
  source_code_hash = "${data.archive_file.lambda_zip1.output_base64sha256}"

}
resource "aws_lambda_function" "test_lambda1" {
  
  function_name = "handler1"
  filename      = "output/test.zip" 
  role          = aws_iam_role.lambda_role.arn
  handler       = "test.handler"
  runtime       = "nodejs12.x"  
  
  layers = [aws_lambda_layer_version.lambda_layer.arn]
   source_code_hash = "${data.archive_file.lambda_zip2.output_base64sha256}"
}
resource "aws_api_gateway_rest_api" "api" {
  name = "myapi"
}
resource "aws_api_gateway_resource" "resource" {
  path_part   = "resource"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.test_lambda.invoke_arn
}
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "apigateway.amazonaws.com"

 # source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
 source_arn = "arn:aws:execute-api:ap-south-1:620215544072:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method.http_method}${aws_api_gateway_resource.resource.path}"
}


resource "aws_api_gateway_resource" "resource1" {
  path_part   = "resource1"
  parent_id   = aws_api_gateway_resource.resource.id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_method" "method1" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.resource1.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration1" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.resource1.id
  http_method             = aws_api_gateway_method.method1.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.test_lambda1.invoke_arn
}
resource "aws_lambda_permission" "apigw_lambda1" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda1.function_name
  principal     = "apigateway.amazonaws.com"

  # source_arn = "arn:aws:execute-api:${var.myregion}:${var.accountId}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method1.http_method}${aws_api_gateway_resource.resource1.path}"
    source_arn = "arn:aws:execute-api:ap-south-1:620215544072:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.method1.http_method}${aws_api_gateway_resource.resource1.path}"

}

# resource "null_resource" "npminstall" {
 # provisioner "local-exec" {
 #   command  ="apt-get install -y npm"
 # }
 # provisioner "local-exec" {
 #   command  ="npm install"
 # }
#}


